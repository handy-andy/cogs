import discord
from discord.ext import commands  # commands extension
import random, io, os
from PIL import Image, ImageSequence, ImageFont, ImageDraw
from lib.CogBase import CogBase


class Auto(CogBase):
    """
        This cog provides the ability to automatically assign roles to new members, and greet them upon join
        The commands provided in this cog allow you to control role and welcome settings
        This cog and it's commands are provided as utility
    """
    def __init__(self, bot):
        super().__init__(bot, 4)
        self.welcome_gif = os.listdir("resources/images/welcome")

    async def __local_check(self, ctx):
        if ctx.guild is None:
            raise commands.NoPrivateMessage('This command cannot be used in private messages.')

        return ctx.channel.permissions_for(ctx.author).administrator or \
            ctx.channel.permissions_for(ctx.author).manage_guild

    @commands.group(brief="Display the current settings for the automatic role assignment")
    async def autorole(self, ctx):
        """
            This command will display your guild's automatic roles in a list
            This command requires the setter to have at least Manage Guild permissions

            Usage:
                {prefix}autorole [set|toggle]
        """
        if ctx.invoked_subcommand is None:
            emb = discord.Embed(title="Auto-role Assignment", description="These are the settings for your guild")
            emb.add_field(name="Enabled", value="<:yes:443358878852317184>" if self.bot.servopts[ctx.guild.id].auto_role_enabled else "<:no:443358878399201280>", inline=True)

            role = discord.utils.get(ctx.guild.roles, id=self.bot.servopts[ctx.guild.id].auto_role_id)
            emb.add_field(name="Role", value=role.mention if role else "Role not set", inline=True)

            await ctx.send(embed=emb)

    @autorole.command(name="roles", brief="View a list of roles that I am allowed to assign")
    async def autorole_roles(self, ctx):
        """
            This command will display the list of roles that the bot has permissions to assign
            This command requires the setter to have at least Manage Guild permissions

            Usage:
                {prefix}autorole roles
        """
        roles = list(filter(lambda r: r.position <= ctx.guild.me.top_role.position, ctx.guild.roles))
        await ctx.send("I can assign these roles:```diff\n%s```" % "\n".join("+ {0.name:<14} ({0.id})".format(role) for role in roles))

    @autorole.command(name="set", brief="Set the role which should be automatically assigned")
    async def autorole_set(self, ctx, role: discord.Role = None):
        """
            This command will overwrite the currently saved role which should be assigned upon joining
            This command requires the setter to have at least Manage Guild permissions

            Passing no role will remove the set role from settings

            Usage:
                {prefix}autorole set [role:role_id]
        """
        if role:
            self.bot.servopts[ctx.guild.id].auto_role_id = role.id
            await ctx.send("Success %s new members will now automatically be assigned to this role!" % ("<a:kms:444271610791198721>" if role else "<:bulbaww:444274429812539402>"))
        else:
            self.bot.servopts[ctx.guild.id].auto_role_id = 0
            await ctx.send("Success %s no role will be assigned upon join!" % ("<a:kms:444271610791198721>" if role else "<:bulbaww:444274429812539402>"))

    @autorole.command(name="toggle", brief="Toggle roles from being assigned")
    async def autorole_toggle(self, ctx):
        """
            This command will toggle whether or not the role should be automatically assigned
            This command requires the setter to have at least Manage Guild permissions

            Usage:
                {prefix}autorole toggle
        """
        value = self.bot.servopts[ctx.guild.id].auto_role_enabled
        self.bot.servopts[ctx.guild.id].auto_role_enabled = 0 if value else 1
        await ctx.send("Success %s I will %s automatically assign the role to new members!" % ("<a:kms:444271610791198721>" if value else "<:bulbaww:444274429812539402>", "no longer" if value else "now"))

    @commands.group(brief="Display the current settings for the automatic role assignment")
    async def welcome(self, ctx):
        """
            This command will display your guild's welcome message settings
            This command requires the setter to have at least Manage Guild permissions

            Usage:
                {prefix}welcome [set|toggle|toggleimage|channel]
        """
        if ctx.invoked_subcommand is None:
            emb = discord.Embed(title="Welcome message", description="These are the settings for your guild")
            emb.add_field(name="Enabled", value="<:yes:443358878852317184>" if self.bot.servopts[ctx.guild.id].auto_welcome_enabled else "<:no:443358878399201280>", inline=True)

            auto_img = self.bot.servopts[ctx.guild.id].auto_welcome_image_enabled
            emb.add_field(name="Image", value="<:yes:443358878852317184>" if auto_img else "<:no:443358878399201280>", inline=True)

            chan = discord.utils.get(ctx.guild.text_channels, id=self.bot.servopts[ctx.guild.id].auto_welcome_channel)
            emb.add_field(name="Channel", value=chan.mention if chan else "Channel not set", inline=True)

            msg = self.bot.servopts[ctx.guild.id].auto_welcome_message
            emb.add_field(name="Welcome message", value=msg if msg != "" else "Message not set")

            await ctx.send(embed=emb)

    @welcome.command(name="set", brief="Set the role which should be automatically assigned")
    async def welcome_set(self, ctx, *, welcome_message: str = None):
        """
            This command will overwrite the currently saved role which should be assigned upon joining
            This command requires the setter to have at least Manage Guild permissions

            Usage:
                {prefix}welcome set <welcome_message:str>

            Note: You can use {{0.name}} or {{0.mention}} to format the welcome message
        """
        if welcome_message:
            self.bot.servopts[ctx.guild.id].auto_welcome_message = welcome_message
            await ctx.send("Success %s new members will now be greeted with this message!" % ("<a:kms:444271610791198721>" if not welcome_message else "<:bulbaww:444274429812539402>"))
        else:
            self.bot.servopts[ctx.guild.id].auto_welcome_message = ""
            await ctx.send("Success %s no message will be sent!" % ("<a:kms:444271610791198721>" if not welcome_message else "<:bulbaww:444274429812539402>"))

    @welcome.command(name="toggle", brief="Toggle welcome messages from being sent")
    async def welcome_toggle(self, ctx):
        """
            This command will toggle whether or not the welcome messages should be sent
            This command requires the setter to have at least Manage Guild permissions

            Usage:
                {prefix}welcome toggle
        """
        value = self.bot.servopts[ctx.guild.id].auto_welcome_enabled
        self.bot.servopts[ctx.guild.id].auto_welcome_enabled = 0 if value else 1
        await ctx.send("Success %s I will %s send the welcome messsages to the set channel!" % ("<a:kms:444271610791198721>" if value else "<:bulbaww:444274429812539402>", "no longer" if value else "now"))

    @welcome.command(name="toggleimage", brief="Toggle welcome image from being sent")
    async def welcome_toggleimage(self, ctx):
        """
            This command will toggle whether or not the welcome images should be sent along with messages
            This command requires the setter to have at least Manage Guild permissions

            Usage:
                {prefix}welcome toggleimage
        """
        value = self.bot.servopts[ctx.guild.id].auto_welcome_image_enabled
        self.bot.servopts[ctx.guild.id].auto_welcome_image_enabled = 0 if value else 1
        await ctx.send("Success %s I will %s send the welcome images to the set channel!" % ("<a:kms:444271610791198721>" if value else "<:bulbaww:444274429812539402>", "no longer" if value else "now"))

    @welcome.command(name="channel", brief="Set the channel for the welcome messages")
    async def welcome_channel(self, ctx, channel: discord.TextChannel = None):
        """
            This command will overwrite the currently saved channel to which the welcome messages should be sent
            If no text channel is provided, it will save the current channel as the trigger channel
            This command requires the setter to have at least Manage Guild permissions

            Usage:
                {prefix}welcome channel [channel:textchannel]
        """
        self.bot.servopts[ctx.guild.id].auto_welcome_channel = channel.id if channel else ctx.channel.id
        chanstr = "this channel" if not channel or channel and ctx.channel.id == channel.id else f"<#{channel.id}>"
        await ctx.send("Success <:bulbaww:444274429812539402> I set %s as the welcome channel!" % chanstr)

    def generate_welcome_image(self, member):
        base = Image.open("./resources/images/welcome/%s" % random.choice(self.welcome_gif))

        fnt = ImageFont.truetype('./resources/fonts/candice.ttf', 30)
        txt = Image.new("RGBA", base.size, (255, 255, 255, 0))

        d = ImageDraw.Draw(txt)
        text = "%s ...\nwelcome!" % member.name
        d.text((4, 5), text, font=fnt, fill=(0, 0, 0, 255))
        d.text((6, 5), text, font=fnt, fill=(0, 0, 0, 255))
        d.text((5, 4), text, font=fnt, fill=(0, 0, 0, 255))
        d.text((5, 6), text, font=fnt, fill=(0, 0, 0, 255))
        d.text((5, 5), text, font=fnt, fill=(255, 255, 255, 255))

        sequence = []
        for frame in ImageSequence.Iterator(base):
            frame = frame.convert("RGBA")

            sequence.append(Image.alpha_composite(frame, txt))

        image_file_object = io.BytesIO()
        sequence[0].save(image_file_object, format="gif", save_all=True, append_images=sequence[1:], loop=0)
        image_file_object.seek(0)

        return image_file_object

    async def on_member_join(self, member):
        chan = discord.utils.get(member.guild.text_channels, id=self.bot.servopts[member.guild.id].auto_welcome_channel)
        msg = self.bot.servopts[member.guild.id].auto_welcome_message

        if self.bot.servopts[member.guild.id].auto_welcome_enabled and chan and msg != "":
            if self.bot.servopts[member.guild.id].auto_welcome_image_enabled:
                image = await self.bot.loop.run_in_executor(None, self.generate_welcome_image, member)
                await chan.send(msg.format(member), file=discord.File(fp=image, filename="welcome.gif"))
            else:
                await chan.send(msg.format(member))

        role = discord.utils.get(member.guild.roles, id=self.bot.servopts[member.guild.id].auto_role_id)
        if self.bot.servopts[member.guild.id].auto_role_enabled and role:
            await member.add_roles(role, reason="Automatically assigned role upon join")

    async def _Auto__error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("You need to present all arguments! Please use `{0.prefix}help {0.command}` "
                           "to get helpful information.".format(ctx))
        elif isinstance(error, discord.Forbidden):
            await ctx.send("<a:kms:444271610791198721> I do not have permissions to perform this action")
        elif isinstance(error, commands.MissingPermissions):
            await ctx.send("<a:kms:444271610791198721> You don't have permissions to perform this action")
        elif isinstance(error, commands.BadArgument):
            await ctx.send("<a:kms:444271610791198721> Please make sure all arguments are of the correct type! "
                           "Use `{0.prefix}help {0.command}` to get helpful information".format(ctx))
        elif not isinstance(error, commands.NoPrivateMessage):
            await ctx.send("<a:kms:444271610791198721> An unknown error has occurred! The admins have been notified")
            await self.bot.send_error("- Server: {0.guild.id}\n- Channel: {0.channel.id}".format(ctx), error)
        else:
            await ctx.send(str(error))


def setup(bot):
    bot.add_cog(Auto(bot))

