import discord  # discord api
from discord.ext import commands  # commands extension

import lib.pysu as osu_lib

from types import SimpleNamespace
from lib.CogBase import CogBase


class Osu(CogBase):
    """
        This cog provides commands for the rhythm game `osu!`
        It interfaces with both the Ripple and Bancho API

        All commands are in the format of:
            {prefix}command [-r] user_name_or_id [-c|-m|-t|-o]

        These switches are optional:
            -r: interface with the Ripple API instead of the Bancho API
            -c: retrieve information for the `Catch the Beat` mode
            -m: retrieve information for the `Mania` mode
            -t: retrieve information for the `Taiko` mode
            -o: retrieve information for the `osu!` mode

        The default mode is `osu!`
    """
    def __init__(self, bot, settings):
        super().__init__(bot, 7, "osu!")
        self.portals = SimpleNamespace(
            bancho=osu_lib.API(key=settings.token, raise_errors=True),
            ripple=osu_lib.API(api_url="https://ripple.moe/api", raise_errors=True)
        )

        self.message_information_cache = {}
        self.mode_list = {
            "-c": osu_lib.modes.ctb, "-o": osu_lib.modes.osu,
            "-m": osu_lib.modes.mania, "-t": osu_lib.modes.taiko
        }
        self.osumoji = settings.emoji

    async def on_reaction_add(self, reaction, user):
        if user.id == self.bot.user.id:
            return

        msg = reaction.message
        if msg.id not in self.message_information_cache:
            return

        await msg.remove_reaction(reaction, user)
        item = self.message_information_cache.get(msg.id, None)

        # max index is 9 for ,osutop (10 items max)
        # max index is 10 for ,osu (player profiler + top 10 recent)
        max_index = 9 if item.type == "osutop" else 10

        if not {"◀": 0 < item.index, "▶": item.index < max_index}.get(reaction.emoji) or \
                item.type == "osu" and item.index > len(item.scores.recent):
            return

        # if emoji == ▶, increment index
        # if emoji == ◀, decrement index
        if reaction.emoji == "▶":
            item.index += 1
        else:
            item.index -= 1

        cback = self.osu_best if item.type == "osutop" else \
            self.osu_profile if item.index == 0 else \
            self.osu_recent

        await cback(msg)

    def mkEmbed(self, embed, fields, user, ripple, which, image=None):
        # this is ugly
        # kill me
        embTitle, embOther = list(embed.items())[0]
        embed = {"title": embTitle, "type": "rich", "color": 0x277ECD}
        if type(embOther) is dict:
            embed = {**embed, **embOther}
        else:
            embed["url"] = embOther

        discord_embed = discord.Embed(**embed)

        discord_embed.set_thumbnail(**{"url": "https://a.%s/%s" % ("ripple.moe" if ripple else "ppy.sh", user.id)})

        for fKey, fVal in iter(fields.items()):
            discord_embed.add_field(**{
                "name": fKey,
                "value": str(fVal),
                "inline": True
            })

        discord_embed.set_author(**{
            "name": user.name,
            "url": "http://%s/u/%s" % ("ripple.moe" if ripple else "osu.ppy.sh", user.id),
            "icon_url": "http://flags.fmcdn.net/data/flags/w580/%s.png" % user.country.lower()
        })

        discord_embed.set_footer(**{
            "text": f"Use ◀ and ▶ to scroll {which} plays",
            "icon_url": "https://s.ppy.sh/images/head-logo.png"
        })

        if image:
            discord_embed.set_image(**{
                "url": image
            })

        return discord_embed

    # Ripple/Osu switcher
    def portal_selector(self, user, ruser, mode):
        # returns ripple?, apiportal, user.name, gamemode
        if user != "-r":
            return False, self.portals.bancho, user, self.mode_list.get(ruser, osu_lib.modes.osu)
        return True, self.portals.ripple, ruser, self.mode_list.get(mode, osu_lib.modes.osu)

    async def osu_profile(self, msg):
        item = self.message_information_cache[msg.id]

        embed = {
            "Profile of %s" % item.user.name: {
                "description": "%i recent plays available" % len(item.scores.recent),
                "url": "http://%s/u/%s" % ("ripple.moe" if item.ripple else "osu.ppy.sh", item.user.id)
            }
        }
        fields = {
            "Rank":         item.user.pp.rank,
            "PP":           item.user.pp.raw,
            "Ranked Score": format(int(item.user.score.ranked), ","),
            "Total score":  format(int(item.user.score.total), ","),
            "Accuracy":     item.user.accuracy_formatted,
            "Playcount":    item.user.count['plays'],
            "Level":        item.user.level,
            "Country Rank": item.user.pp.country_rank
        }

        bestplays = []
        for index in range(3):
            if index > (len(item.scores.top) - 1):
                continue

            play = item.scores.top[index]

            if not play.bm:
                bestplays.append(
                    "{0} <beatmap not found> {1} ({2.accuracy}) **{0.pp:.2f}** (*{0.pp_weighted:.2f}*)".format(
                        self.osumoji.get(play.rank, play.rank), '' if play.mods == 'None' else '+%s' % play.mods, play
                    )
                )
                continue

            bestplays.append(
                "{1} {0.bm.artist} - {0.bm.title} [{0.bm.version}] {2} ({0.accuracy}) "
                "**{0.pp:.2f}** (*{0.pp_weighted:.2f}pp*)".format(
                    play, self.osumoji.get(play.rank, play.rank),
                    '' if play.mods == 'None' else '+%s' % play.mods
                )
            )

        fields["Best Plays"] = "\n".join(bestplays) if len(bestplays) > 0 else "None available"

        await msg.edit(content="", embed=self.mkEmbed(embed, fields, item.user, item.ripple, "recent"))

    async def osu_recent(self, msg):
        item = self.message_information_cache[msg.id]
        index = item.index - 1

        recent = item.scores.recent[index] or SimpleNamespace(mods="unknown", beatmap_id="unknown", bm=None)

        image = None
        if recent.bm:
            image = "https://assets.ppy.sh/beatmaps/%i/covers/cover.jpg" % recent.bm.set_id
            embed = {
                ("Recent play #%i" % (index + 1)): {
                    "description": "%s - %s [%s]\n\n**Using mods:** %s\n**Played at UTC:** %s" % (
                        recent.bm.artist, recent.bm.title, recent.bm.version, recent.mods, recent.date
                    ),
                    "url": "http://osu.ppy.sh/b/%i" % recent.bm.id
                }
            }
        else:
            embed = {
                ("Recent play #%i" % (index + 1)): {
                    "description": "The beatmap for this song could not be found.\n\n**Using mods:**\n%s" % recent.mods,
                    "url": "http://osu.ppy.sh/b/%i" % recent.beatmap_id
                }
            }

        fields = {
            "Score":    format(recent.score, ","),
            "Combo":    str(recent.maxCombo) + (" - Perfect!" if recent.perfect else ""),
            "Accuracy": recent.accuracy,
            "Miss":     recent.count.miss,
            "Rank":     self.osumoji.get(recent.rank, recent.rank),
        } if recent.bm else {}

        await msg.edit(content="", embed=self.mkEmbed(embed, fields, item.user, item.ripple, "recent", image))

    async def osu_best(self, msg):
        item = self.message_information_cache[msg.id]
        score = item.scores[item.index]

        if score.bm:
            image = "https://assets.ppy.sh/beatmaps/%i/covers/cover.jpg" % score.bm.set_id
            embed = {
                ("Top Score #%i" % (item.index+1)): {
                    "description": "%s - %s [%s]\n\n**Using mods:** %s\n**Played at UTC:** %s" % (
                        score.bm.artist, score.bm.title, score.bm.version, score.mods, score.date
                    ),
                    "url": "http://osu.ppy.sh/b/%i" % score.bm.id
                }
            }
        else:
            image = None
            embed = {
                ("Top Score #%i" % (item.index+1)): {
                    "description": "The beatmap for this song could not be found.\n\n**Using mods:**\n%s" % score.mods,
                    "url": "http://osu.ppy.sh/b/%i" % score.beatmap_id
                }
            }

        fields = {
            "Score":    format(score.score, ","),
            "Combo":    str(score.maxCombo) + (" - Perfect!" if score.perfect else ""),
            "Accuracy": score.accuracy,
            "Miss":     score.count.miss,
            "Rank":     self.osumoji.get(score.rank, score.rank),
            "PP":       f"{score.pp:.2f}\n*{score.pp_weighted:.2f} weighted*"
        }

        emb = self.mkEmbed(embed, fields, item.user, item.ripple, "top", image)
        await msg.edit(content="", embed=emb)

    @commands.command(brief="Retrieves your osu! profile")
    async def osu(self, ctx, user, ruser=None, mode=None):
        """
            Gets your osu stats and recent plays from either bancho or ripple.

            Use `{prefix}osu -r <profile>` to get ripple profile
            Use `{prefix}osu <profile>` to get bancho profile

            These switches are optional:
                -r: interface with the Ripple API instead of the Bancho API
                -c: retrieve information for the `Catch the Beat` mode
                -m: retrieve information for the `Mania` mode
                -t: retrieve information for the `Taiko` mode
                -o: retrieve information for the `osu!` mode

            The default mode is `osu!`

            Examples:
                `{prefix}osu <profile> -c`: CTB on Bancho
                `{prefix}osu -r <profile> -m`: Mania on Ripple
        """
        ripple, portal, user, mode = self.portal_selector(user, ruser, mode)

        if not user:
            raise commands.MissingRequiredArgument("User required")

        msg = await ctx.send("<a:typing:483286304537182208>")
        user = await portal.get_user(user, m=mode)

        async def get_beatmap_info(scores):
            for score in scores:
                try:
                    item = await portal.get_beatmap(score.beatmap_id, m=mode)
                except osu_lib.PysuError:
                    item = None
                finally:
                    setattr(score, "pp_weighted", score.pp * 0.95 ** scores.index(score))
                    setattr(score, "bm", item)

        try:
            user_score = await portal.get_user_best(user.name, limit=3, m=mode)
            await get_beatmap_info(user_score)
        except osu_lib.PysuError:
            user_score = []

        try:
            recent = await portal.get_user_recent(user.name, m=mode)
            await get_beatmap_info(recent)
        except osu_lib.PysuError:
            recent = []

        self.message_information_cache[msg.id] = SimpleNamespace(
            type="osu", index=0,
            mode=mode, user=user,
            ripple=ripple, portal=portal,
            scores=SimpleNamespace(
                recent=recent,
                top=user_score
            )
        )

        await self.osu_profile(msg)
        await msg.add_reaction("◀")
        await msg.add_reaction("▶")

    @commands.command(brief="Retrieves your top 10 osu! scores")
    async def osutop(self, ctx, user, ruser=None, mode=None):
        """
            Gets your best 10 beatmap scores from either bancho or ripple.

            Use `{prefix}osutop -r <profile>` to get ripple profile
            Use `{prefix}osutop <profile>` to get bancho profile

            These switches are optional:
                -r: interface with the Ripple API instead of the Bancho API
                -c: retrieve information for the `Catch the Beat` mode
                -m: retrieve information for the `Mania` mode
                -t: retrieve information for the `Taiko` mode
                -o: retrieve information for the `osu!` mode

            The default mode is `osu!`

            Examples:
                `{prefix}osutop <profile> -c`: CTB on Bancho
                `{prefix}osutop -r <profile> -m`: Mania on Ripple
        """
        ripple, portal, user, mode = self.portal_selector(user, ruser, mode)
        if not user:
            raise commands.MissingRequiredArgument("User required")

        msg = await ctx.send("<a:typing:483286304537182208>")
        user = await portal.get_user(user, m=mode)

        try:
            user_score = await portal.get_user_best(user.name, limit=10, m=mode)
            for score in user_score:
                try:
                    item = await portal.get_beatmap(score.beatmap_id, m=mode)
                except osu_lib.PysuError:
                    item = None
                finally:
                    setattr(score, "pp_weighted", score.pp * 0.95 ** user_score.index(score))
                    setattr(score, "bm", item)
        except osu_lib.PysuError:
            user_score = []

        self.message_information_cache[msg.id] = SimpleNamespace(
            type="osutop", index=0,
            mode=mode, user=user,
            ripple=ripple, portal=portal,
            scores=user_score
        )
        await self.osu_best(msg)
        await msg.add_reaction("◀")
        await msg.add_reaction("▶")

    # Handle errors
    async def _Osu__error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("You need to specify a user, friend")
        elif isinstance(error, osu_lib.PysuError):
            original_message = str(error.original)
            if "User" in original_message:
                await ctx.send("The user you wanted to check does not exist.")
            elif "Beatmap" in original_message:
                await ctx.send("This beatmap is invalid")
        else:
            await ctx.send(":thumbsdown: An unknown error has occured! The admins have been notified of this")
            await self.bot.send_error("- Server: {0.guild.id}\n- Channel: {0.channel.id}".format(ctx), error)


def setup(bot, settings):
    bot.add_cog(Osu(bot, settings))
