import discord
from discord.ext import commands  # commands extension
import io, textwrap, os, random

from PIL import Image, ImageSequence, ImageFont, ImageDraw
from types import SimpleNamespace
from lib.CogBase import CogBase


class Images(CogBase):
    """Random commands"""
    def __init__(self, bot):
        super().__init__(bot, 9, "Images")
        self.limit = {}

        self.img = SimpleNamespace(
            hugs   = os.listdir("resources/images/hug"),
            kisses = os.listdir("resources/images/kiss")
        )

    def genimg(self, text, img):
        base = Image.open('./resources/images/%s'%img)
        y = base.height

        fnt = ImageFont.truetype('./resources/fonts/candice.ttf', 20)
        txt = Image.new("RGBA", base.size, (255,255,255,0))

        d = ImageDraw.Draw(txt)
        d.text((4, y - 42), text, font=fnt, fill=(0,0,0,255))
        d.text((6, y - 42), text, font=fnt, fill=(0,0,0,255))
        d.text((5, y - 43), text, font=fnt, fill=(0,0,0,255))
        d.text((5, y - 41), text, font=fnt, fill=(0,0,0,255))
        d.text((5, y - 42), text, font=fnt, fill=(255,255,255,255))

        sequence = []
        for frame in ImageSequence.Iterator(base):
            frame = frame.convert("RGBA")
            sequence.append(Image.alpha_composite(frame, txt))

        image_file_object = io.BytesIO()
        sequence[0].save(image_file_object, format="gif", save_all=True, append_images=sequence[1:], loop=0)
        image_file_object.seek(0)

        return image_file_object

    def gencustimg(self, text, img):
        base = Image.open('./resources/images/%s'%img)
        y = base.height

        fnt = ImageFont.truetype('./resources/fonts/impact.ttf', 20)
        txt = Image.new("RGBA", base.size, (255,255,255,0))

        d = ImageDraw.Draw(txt)
        W, H = base.size
        text = textwrap.wrap(text, width=40)
        current_h = 5
        for line in text:
            w, h = d.textsize(line, font=fnt)
            d.text(((W-w)/2-1, current_h), line, font=fnt, fill=(0,0,0,255))
            d.text(((W-w)/2+1, current_h), line, font=fnt, fill=(0,0,0,255))
            d.text(((W-w)/2,   current_h + 1), line, font=fnt, fill=(0,0,0,255))
            d.text(((W-w)/2,   current_h - 1), line, font=fnt, fill=(0,0,0,255))
            d.text(((W-w)/2,   current_h), line, font=fnt, fill=(255,255,255,255))
            current_h += h + 3

        image_file_object = io.BytesIO()

        if base.format == "GIF":
            sequence = []
            for frame in ImageSequence.Iterator(base):
                frame = frame.convert("RGBA")
                sequence.append(Image.alpha_composite(frame, txt))
            
            sequence[0].save(image_file_object, format="gif", save_all=True, append_images=sequence[1:], loop=0)
        else:
            img = base.convert("RGBA")
            img = Image.alpha_composite(img, txt)
            img.save(image_file_object, format=base.format)

        image_file_object.seek(0)

        return image_file_object

    @commands.command(brief="Me_irl")
    @commands.guild_only()
    async def meirl(self, ctx, *, text: str = None):
        if text and len(text) > 150:
            return await ctx.send("Your text is too long :c")
        image = await self.bot.loop.run_in_executor(None, self.gencustimg, text if text else "no matter how I look at it, it's your fault that I'm not popular!", "memes/meirl.gif")
        await ctx.send(file=discord.File(fp=image, filename="me_irl.gif"))

    @commands.command(brief="True Happiness")
    @commands.guild_only()
    async def happy(self, ctx, *, text: str = None):
        if text and len(text) > 150:
            return await ctx.send("Your text is too long :c")
        image = await self.bot.loop.run_in_executor(None, self.gencustimg, text if text else "You found .. me ..", "memes/happy.png")
        await ctx.send(file=discord.File(fp=image, filename="happy.png"))

    @commands.command(brief="Better luck next time")
    @commands.guild_only()
    async def sad(self, ctx, *, text: str = None):
        if text and len(text) > 150:
            return await ctx.send("Your text is too long :c")
        image = await self.bot.loop.run_in_executor(None, self.gencustimg, text if text else "bleh.", "memes/sad.png")
        await ctx.send(file=discord.File(fp=image, filename="sad.png"))

    @commands.command(brief="*hugs*")
    @commands.guild_only()
    async def hug(self, ctx, member: discord.Member = None):
        image = await self.bot.loop.run_in_executor(None, self.genimg, "%s hugged\n%s" % (ctx.author.name, (member or self.bot.user).name), "hug/%s"%random.choice(self.img.hugs))
        await ctx.send(file=discord.File(fp=image, filename="hug.gif"))

    @commands.command(brief="*kisses*")
    @commands.guild_only()
    async def kiss(self, ctx, member: discord.Member = None):
        image = await self.bot.loop.run_in_executor(None, self.genimg, "%s kissed\n%s" % (ctx.author.name, (member or self.bot.user).name), "kiss/%s"%random.choice(self.img.kisses))
        await ctx.send(file=discord.File(fp=image, filename="kiss.gif"))

    @commands.command(brief="<3", name="❤", aliases=['<3', '\<3'], hidden=True)
    async def _heart(self, ctx):
        if ctx.prefix[:-1] == self.bot.user.mention:
            image = await self.bot.loop.run_in_executor(None, self.genimg, "%s hugged\n%s" % (self.bot.user.name, ctx.author.name), "hug/%s"%random.choice(self.img.hugs))
            await ctx.send(file=discord.File(fp=image, filename="hug.gif"))


def setup(bot):
    bot.add_cog(Images(bot))
