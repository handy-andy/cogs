import discord  # discord api
from discord.ext import commands  # commands extension
import asyncio
import random
from lib.checks import is_owner


class Owner:
    def __init__(self, bot):
        self.bot = bot
        self.position = 1

    async def __local_check(self, message):
        return is_owner(message)

    # this command requires PM2 nodejs module to stay alive
    @commands.command()
    async def reboot(self, ctx):
        """ Reboot the bot """
        await ctx.send('Rebooting now...')
        await asyncio.sleep(1)
        await self.bot.logout()

    @commands.command()
    async def reload(self, ctx, name: str):
        """Reload a cog"""
        self.bot.unload_extension(f"cogs.{name}")
        self.bot.load_extension(f"cogs.{name}")
        await ctx.send(f"Reloaded extension **{name}.py**")

    @commands.command(name="load_cog")
    async def oload(self, ctx, name: str):
        """Load a cog"""
        self.bot.load_extension(f"cogs.{name}")
        await ctx.send(f"Loaded extension **{name}.py**")

    @commands.command(name="unload_cog")
    async def ounload(self, ctx, name: str):
        """Unload a cog"""
        self.bot.unload_extension(f"cogs.{name}")
        await ctx.send(f"Unloaded extension **{name}.py**")

    @commands.command(name="view_servers")
    async def checkservers(self, ctx):
        """Unload a cog"""
        await ctx.send("Found %i servers:\n- %s" %
                       (len(self.bot.guilds), "\n- ".join([g.name for g in self.bot.guilds])))

    @commands.command(name="generate_server_invite")
    async def gen_invite(self, ctx, server_id: int):
        """Generate invite to server ID"""
        guild = discord.utils.get(self.bot.guilds, id=server_id)
        if guild:
            invite = await random.choice(guild.text_channels).create_invite(max_age=0, max_uses=1)
            await ctx.send(invite.url)

    @commands.command(name="channel_permissions")
    async def chanperms(self, ctx, server_id: int = None):
        """View channel perms by server ID"""
        guild = discord.utils.get(self.bot.guilds, id=server_id) if server_id else ctx.guild
        if guild:
            content = []
            for chan in self.bot.servopts[guild.id].channels_restricted:
                channel = discord.utils.get(guild.text_channels, id=chan)
                if channel:
                    content.append((
                        "**{0.name}**:\n"
                        "<:yes:443358878852317184> Restricted"
                    ).format(channel))

            await ctx.send("\n".join(content) if len(content) > 0 else "None restricted")

    async def _Owner__error(self, ctx, err):
        await ctx.send(f"```diff\n- {err}```")


def setup(bot):
    bot.add_cog(Owner(bot))

