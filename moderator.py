import discord  # discord api
from discord.ext import commands  # commands extension
import datetime, asyncio, re
from lib.CogBase import CogBase
import errors


class Moderation(CogBase):
    """
        This cog provides commands which enable server managers to control member activity
        They are provided as utility as to make manager's lives better
    """
    def __init__(self, bot):
        super().__init__(bot, 3, "Moderation")

        self.mention_regex = re.compile("<@!?([0-9]+)>")
        bot.loop.create_task(self.autounmute_recall())

    async def __local_check(self, ctx):
        if ctx.guild is None:
            raise commands.NoPrivateMessage('This command cannot be used in private messages.')

        return ctx.channel.permissions_for(ctx.author).administrator or all(p(ctx) for p in ctx.command.checks)

    async def autounmute_recall(self):
        if not getattr(self.bot, "servopts", None):
            await asyncio.sleep(5.0)
            asyncio.ensure_future(self.autounmute_recall())
            return

        removed = []
        for server_id, opts in self.bot.servopts.items():
            guild = discord.utils.get(self.bot.guilds, id=int(server_id))
            if opts.no_mute or not guild:
                continue

            for index, item in enumerate(opts.muted):
                if datetime.datetime.now() < item["muted_time"]:
                    continue

                user = discord.utils.get(guild.members, id=int(item["muted_id"]))
                if user:
                    role = discord.utils.get(guild.roles, name="Muted")
                    await user.remove_roles(role, reason="Automatic unmute")

                removed.append(index)

            for item in removed:
                if opts.muted.get_index(item, None):
                    # remove from database
                    opts.muted.remove(opts.muted[item])

        await asyncio.sleep(5.0)
        asyncio.ensure_future(self.autounmute_recall())

    async def create_Muted(self, server):
        try:
            # server permissions for current user
            perms = server.me.guild_permissions

            # get the Muted role
            role = discord.utils.get(server.roles, name="Muted")

            # If no muted role on the server, create the role, with permissions for sending messages disabled
            if not role:
                await self.bot.send_status("+ Creating Muted role on server {0.name}...".format(server))
                objPermissions = discord.Permissions()
                objPermissions.update(send_messages=False)
                role = await server.create_role(name="Muted", permissions=objPermissions)

            await asyncio.sleep(1)

            # loop over text channels in current server
            for channel in server.text_channels:
                # if role exists (set above), and we have permissions to manage channels,
                # and the Muted role was *not* already modified, then overwrite permissions for the channel
                if role and perms.manage_channels and not discord.utils.get(channel.changed_roles, name="Muted"):
                    await self.bot.send_status("+ Modifying channel #{0.name} on server {1.name} to have Muted permissions...".format(channel, server))
                    objPermissions = discord.PermissionOverwrite()
                    objPermissions.send_messages = False
                    await channel.set_permissions(role, overwrite=objPermissions)
        except BaseException as e:
            self.bot.servopts[server.id].no_mute = True
            await self.bot.send_error(f"- Error creating Muted role on server {server.name}", e)

    async def on_ready(self):
        # update roles
        for guild in self.bot.guilds:
            await self.create_Muted(guild)

    async def on_guild_join(self, guild):
        await self.create_Muted(guild)

    @commands.command(brief="Kick a member from your guild")
    @commands.has_permissions(kick_members=True)
    @commands.bot_has_permissions(kick_members=True)
    async def kick(self, ctx, member: discord.Member):
        """
            This command forcefully removes the mentioned member from your guild
            This command requires the setter to have at least Kick Members permissions

            Usage:
                {prefix}kick <member:mention>
        """
        userNick = member.display_name
        await member.kick()
        await ctx.send("bye bye %s !" % userNick)

    @commands.command(brief="Ban a member from your guild")
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def ban(self, ctx, member: discord.Member):
        """
            This command forcefully removes the mentioned member from your guild, and disallows them from re-joining
            This command requires the setter to have at least Ban Members permissions

            Usage:
                {prefix}ban <member:mention>
        """
        userNick = member.display_name
        await member.ban()
        await ctx.send("bye bye %s !" % userNick)

    @commands.command(brief="Mute a member of your guild")
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def mute(self, ctx, member: discord.Member, *, mute_args=None):
        """
            This command creates a timed mute of a mentioned member, defaulting to 26 weeks.
            This command requires the setter to have at least Manage Roles permissions

            Usage:
                {prefix}mute <member:mention> [length:number <s|m|h|d|w>] [reason:text]
        """
        if len(ctx.message.mentions) == 0:
            return ctx.send("You must specify one user to mention")

        if not member:
            return ctx.send("You need to mention someone in this guild to mute")

        args = mute_args.split(" ")

        channel = False
        if args[0] == "-c":
            del args[0]
            channel = True

        mute_reason = " ".join(mute_args)

        if not self.bot.db.exists("dino_server_muted", {"muted_id": member.id}):
            kwargs = {}  # for timedelta
            reason = "{0.name}#{0.discriminator} has been muted by {1.name}#{1.discriminator}".format(member, ctx.author)
            mute_time = None
            if mute_reason:
                mute_reason = mute_reason.split(" ")

                length = {"s": "seconds", "m": "minutes", "h": "hours", "d": "days", "w": "weeks"}
                if len(mute_reason) > 1 and mute_reason[0].isdigit():
                    if mute_reason[1] not in length:
                        return await ctx.send("Invalid time format! Please check the help document for this command.")
                    kwargs[length[mute_reason[1]]] = int(mute_reason[0])
                    mute_time = f"{mute_reason[0]} {length[mute_reason[1]]}"
                    mute_reason = mute_reason[2:]
                else:
                    kwargs["weeks"] = 26

                if len(mute_reason) > 0:
                    mute_reason = " ".join(mute_reason)
                    reason += ". Reason: %s" % (" ".join(mute_reason))
            else:
                kwargs["weeks"] = 26

            self.bot.servopts[ctx.guild.id].muted.append({
                "muted_id": member.id,
                "muted_time": (datetime.datetime.now() + datetime.timedelta(**kwargs)).replace(microsecond=0)
            })

            await ctx.send("%s has been muted for %s: %s" % (
                member.display_name, mute_time or "the following reason", mute_reason or "None"
            ))

    @commands.command(brief="Unmute a member of your guild")
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def unmute(self, ctx, member: discord.Member):
        """
            This command removes the Muted role as well as the timer from the database.
            This command requires the setter to have at least Manage Roles permissions

            Usage:
                {prefix}unmute <member:mention>
        """
        items = self.bot.servopts[ctx.guild.id].muted
        item = [item for item in items if item["muted_id"] == member.id]
        item = item[0] if len(item) > 0 else None

        if item:
            reason = "{0.name}#{0.discriminator} has been unmuted by {1.name}#{1.discriminator}".format(member, ctx.author)
            items.remove(item)
            role = discord.utils.get(ctx.guild.roles, name="Muted")
            await member.remove_roles(role, reason = reason)
            await ctx.send("%s has been unmuted!" % member.display_name)
        else:
            await ctx.send("%s is not muted!" % member.display_name)

    @commands.command(brief="Remove an amount of messages from the channel")
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    async def prune(self, ctx, amount: int, member: discord.Member = None):
        """
            This command prunes the amount of messages you pass from the selected channel.
            It can optionally remove that amount of messages from an individual member.
            This command requires the setter to have at least Manage Messages permissions

            Usage:
                {prefix}prune <amount:number> [member:mention]
        """
        if not member:
            deleted = await ctx.channel.purge(limit=amount)
            del_messages = await ctx.send(f"{len(deleted)} messages by {ctx.author.mention} has been removed")
            await asyncio.sleep(3)
            try:
                await del_messages.delete()
            except discord.NotFound:
                pass
        else:
            delete_list = []
            async for msg in ctx.message.channel.history(limit=amount*5):
                if len(delete_list) == amount:
                    break
                if member.id == msg.author.id:
                    delete_list.append(msg)

            if len(delete_list) == 1:
                try:
                    await delete_list[0].delete()
                except discord.NotFound:
                    pass
            else:
                for i in range(len(delete_list) // 100 + 1):
                    try:
                        await ctx.message.channel.delete_messages(delete_list[100*i : 100*(i + 1)])
                    except discord.NotFound:
                        pass

                    await asyncio.sleep(0.5)

    async def _Moderation__error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("You need to present all arguments! Please use `{0.prefix}help {0.command}` to get helpful information.".format(ctx))
        elif isinstance(error, discord.Forbidden):
            await ctx.send(":thumbsdown: I do not have permissions to perform this action ;_;")
        elif isinstance(error, commands.MissingPermissions):
            await ctx.send(":thumbsdown: You don't have permissions to perform this action ;_;")
        elif isinstance(error, commands.BadArgument):
            await ctx.send(":thumbsdown: Please make sure all arguments are of the correct type! Use `{0.prefix}help {0.command}` to get helpful information".format(ctx))
        elif not isinstance(error, errors.ModuleDisabled) or not isinstance(error, errors.ChanRestricted):
            await ctx.send(":thumbsdown: An unknown error has occured! The admins have been notified of this")
            await self.bot.send_error("- Server: {0.guild.id}\n- Channel: {0.channel.id}".format(ctx), error)


def setup(bot):
    bot.add_cog(Moderation(bot))
