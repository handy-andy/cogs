import discord
from discord.ext import commands  # commands extension

import random, json, datetime, asyncio, re
import aiohttp, async_timeout, dice
import urllib.parse
from datetime import date
from lib.cleverbot import Cleverbot
from types import SimpleNamespace
from lib.argtypes import upper_str
from lib.CogBase import CogBase


class Random(CogBase):
    """Random commands"""
    def __init__(self, bot):
        super().__init__(bot, 8, "Random")
        self.limit = {}

        self.json = SimpleNamespace(
            cookie=json.load(open("resources/data/cookie.json", encoding="utf8")),
            factlist=json.load(open("resources/data/facts.json", encoding="utf8")),
            animals=json.load(open("resources/data/animal.json", encoding="utf8")),
            hello=json.load(open("resources/data/helloo.json", encoding="utf8")),
            m8ball=json.load(open("resources/data/8ball.json", encoding="utf8"))
        )

        self.twohundrediq = Cleverbot("gXwCOggM9Y9PUIhM", "xjeC6RWdpA68PGCItovDPn22PkHLi30a")
        self.twohundrediq.setNick("rcXG9eBt")
        asyncio.ensure_future(self.twohundrediq.create(), loop=self.bot.loop)

        self.currencies = {}
        bot.loop.create_task(self.update_currency_rates())

    @commands.command(name="cookie", brief="fififi")
    async def _cookie(self, ctx):
        await ctx.send(
            "*cracks the cookie and looking at the letter inside, promising..*: ** %s ** " % (
                random.choice(self.json.cookie)
            )
        )

    @commands.command(brief="Flip a coin!")
    async def coinflip(self, ctx):
        await ctx.send(
            "*flips the coin, hoping on receiving the right thing...* **you received %s !** " % (
                random.choice(["tails", "heads"])
            )
        )

    @commands.command(brief="Greet the Dino!")
    async def hi(self, ctx):
        d = date.today()
        random.seed(int("{0}{1.day}{1.month}{1.year}".format(ctx.author.id, d)))
        await ctx.send(
            "Hello %s! You seem to be super %s today!" % (
                ctx.author.name, random.choice(self.json.hello)
            )
        )

    @commands.command(name="8ball", brief="Magic 8ball!")
    async def eightball(self, ctx):
        async with ctx.typing():
            await asyncio.sleep(2)
            await ctx.send(random.choice(self.json.m8ball))

    @commands.command(brief="Ask the dino a question!")
    async def ask(self, ctx, *, question: str = ""):
        """
            Ask the bot a question

            Usage:
                {prefix}ask <question...:text>
        """
        if question == "":
            return await ctx.send("You have to ask me a question %s-san!" % ctx.author.mention)

        async with ctx.typing():
            response = await self.twohundrediq.ask(question)
            await ctx.send(response.capitalize() if response else "I don't understand :/")

    @commands.command(brief="Receive a random number!")
    async def roll(self, ctx, lower: str = "", upper: str = ""):
        """
            Roll a random number

            Default lower value is 1
            Default upper value is 100

            If lower is die notation, it will roll a dice

            Usage:
                {prefix}roll [lower] [upper]
        """
        try:
            rolled = sum(dice.roll(lower))
        except:
            if not lower.isdigit():
                lower = 1
            if not upper.isdigit():
                upper = 100

            lower = int(lower)
            upper = int(upper)

            if lower < 1:
                lower = 1

            if lower > 1 and upper == 100:
                upper = lower
                lower = 1

            rolled = random.randint(lower, upper)

        await ctx.send("%s has rolled %s points!" % (ctx.author.mention, rolled))

    @commands.command(brief="Expand your knowledge about nonsense!")
    async def facts(self, ctx):
        """
            Retrieve a random fact from our list

            Usage:
                {prefix}facts
        """
        await ctx.send(random.choice(self.json.factlist))

    async def on_message(self, message):
        if not self.bot.ready or not message.guild or message.author.bot:
            return

        if message.channel.id in self.bot.servopts[message.guild.id].channels_restricted:
            return

        # split the message so we can parse it
        msg = message.content.split(" ")
        # if message is a dadbot or tableflip, set timer for entire guild
        im = msg[0].lower() in ["im", "i'm"] or " ".join(msg[:2]).lower() == "i am"

        if im or message.content == "(╯°□°）╯︵ ┻━┻":
            if message.guild.id in self.limit and\
               self.limit[message.guild.id] + datetime.timedelta(hours=24) >= datetime.datetime.now():
                return

            self.limit[message.guild.id] = datetime.datetime.now()

        # dadbot
        if im:
            if " ".join(msg[:2]).lower() == "i am":
                # if first are "i am", delete both "i" and "am"
                del msg[:2]
            else:
                # else just remove first word
                del msg[0]

            msg = " ".join(msg)
            msg = re.split('[,;.]', msg)[0]
            if msg:
                # send dadbot message huehuehue
                await message.channel.send(content="hi %s, i'm Handy Andy!" % msg)
            else:
                # message was empty
                del self.limit[message.guild.id]

        # if table has been flipped, unflip table
        if message.content == "(╯°□°）╯︵ ┻━┻":
            await message.channel.send(content="┬─┬ ノ( ゜-゜ノ)")

    @commands.command(brief="What is your spirit animal?")
    async def animal(self, ctx):
        special_ids = {
            232191905394327562: ["Whippet", "dog"],
            412282735965634562: ["Whippet", "dog"],
            221893285759811584: ["Dino"],
            150417106549211136: ["Red Panda", "mammal"],
            303677485797605386: ["Red Panda", "mammal"],
            179596539797307392: ["Llama", "mammal"],
            130754548133265408: ["Goose", "bird"],
            242410391613865984: ["Doge", "mammal"],
            327534523560624139: ["Black Panther", "mammal"],
            382940809533587477: ["Otter", "mammal"],
            218415631479996417: ["Red Fox", "mammal"],
            457338914794110999: ["Bearcat"]
        }

        random.seed(ctx.author.id)
        animal = special_ids.get(ctx.author.id, random.choice(self.json.animals))
        random.seed()

        async with aiohttp.ClientSession() as session:
            async with async_timeout.timeout(15):
                async with session.get("https://pixabay.com/api/?key=10410024-8db48c67e2a37e4cfcc085a63&q=%s" % " ".join(animal)) as response:
                    res = await response.json()
                    emb = discord.Embed()
                    emb.set_image(url=random.choice(res["hits"])["largeImageURL"])

        await ctx.send("Your spirit animal is a%s %s" % ('n' if animal[0][:1] in "aeiou" else '', animal[0]), embed=emb)

    async def update_currency_rates(self):
        async with aiohttp.ClientSession() as session:
            async with async_timeout.timeout(15):
                async with session.get("http://www.apilayer.net/api/live?access_key=c4bf3052c0881bbcaa3d16508a48004d") as response:
                    self.currencies = (await response.json())["quotes"]

        await asyncio.sleep(86400)  # 24h exactly
        await self.update_currency_rates()

    @commands.command(brief="Do a currency conversion")
    async def convert(self, ctx, amount, from_currency: upper_str, to_currency: upper_str):
        if len(from_currency) != 3 or len(to_currency) != 3:
            return await ctx.send("The currency you sent needs to be 3 letters characters long")

        try:
            amount = float(amount)
            converted = 0

            if from_currency != "USD":
                converted = amount / self.currencies["USD"+from_currency]

            if to_currency != "USD":
                converted = (converted if converted > 0 else amount) * self.currencies["USD"+to_currency]
        except ValueError:
            await ctx.send("The amount needs to be a number!")
        except KeyError as e:
            await ctx.send("The currency you want to convert %sis invalid, please choose a valid currency" %
                           ("to " if str(e)[-4:-1] == to_currency else ""))
        else:
            await ctx.send("That amount is " + format(converted, '3,.2f') + " " + to_currency)

    @commands.command(brief="What language is this?")
    async def lang(self, ctx, *, item: str):
        async with ctx.typing():
            async with aiohttp.ClientSession() as session:
                async with async_timeout.timeout(15):
                    async with session.get(
                            "http://apilayer.net/api/detect?"
                            "access_key=26db8ef2b5f095a257a18f1680323d04"
                            "&query=" + urllib.parse.quote(item)) as response:
                        languages = (await response.json())["results"]

            message = ""
            for language in languages:
                message += f"{language['language_name']} ({language['probability']:.2f}% probability," \
                           f"{language['percentage']:.2f}% match)\n"

            await ctx.send(message)

    async def _Random__error(self, ctx, err):
        await self.bot.send_error(ctx, err)


def setup(bot):
    bot.add_cog(Random(bot))
