class Ducky:
    def __init__(self, bot):
        self.bot = bot
        self.name = "Ducky's cog"
        self.position = 9
        self.strikes = {}

    async def on_message(self, message):
        if (
            message.guild and
            message.guild.id == 458609613823148032 and
            message.author.id == 382940809533587477
        ):
            if len(message.mentions) > 0:
                if message.content[:13].upper() == "AVADA KEDAVRA":
                    for m in message.mentions:
                        # ban
                        try:
                            await m.ban()
                        except:
                            await message.channel.send("It would seem like there is... an issue... with banning this "
                                                       "user... Why don't you try again, young man, instead of wasting "
                                                       "your time mucking about?")
                        else:
                            await message.channel.send(f"AVADA KEDAVRA {m.mention}!!!!!")
                elif message.content[:12].upper() == "SECTUMSEMPRA":
                    # kick
                    for m in message.mentions:
                        try:
                            await m.kick()
                        except:
                            await message.channel.send("It would seem like there is... an issue... with kicking this "
                                                       "user... Why don't you try again, young man, instead of wasting "
                                                       "your time mucking about?")
                        else:
                            await message.channel.send(f"SECTUMSEMPRA {m.mention}!!!!!")
                elif message.content[:9].upper() == "CONFRINGO":
                    # add strikes
                    for m in message.mentions:
                        self.strikes[m.id] = self.strikes.get(m.id, 0) + 1
                elif message.content[:18].upper() == "PETRIFICUS TOTALUS":
                    # timeout user
                    pass
                elif message.content[:7].upper() == "STUPEFY":
                    # mute
                    pass


def setup(bot):
    bot.add_cog(Ducky(bot))
