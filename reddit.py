import discord  # discord api
from discord.ext import commands  # commands extension

import random
import praw

from lib.CogBase import CogBase
from types import SimpleNamespace
import time


class RedditSub:
    def __init__(self, reddit, sub):
        self._sub = reddit.subreddit(sub)
        self._sub_cache = None
        self._last_update = None

    @property
    def get_rnd_item(self):
        if not self._last_update or self._last_update > time.time() + 300:
            self._update_cache()
            self._last_update = time.time()

        embed = discord.Embed()
        embed.set_image(url=random.choice(self._sub_cache))
        return embed

    def _update_cache(self):
        reddit = list(self._sub.new(limit=100))

        self._sub_cache = []
        for child in reddit:
            if getattr(child, "post_hint", "not image") == "image":
                self._sub_cache.append(child.url)


class Reddit(CogBase):
    """
        This cog provides commands which retrieve random posts from certain subreddits
        They are provided as a means to relieve your stress with fun and happiness
    """
    def __init__(self, bot, settings):
        super().__init__(bot, 7, "Reddit")

        reddit = praw.Reddit(
            user_agent="Discord Bot (by /u/gear4)",
            client_id=settings.client_id, client_secret=settings.client_secret,
            username=settings.username, password=settings.password
        )

        self.sub_list = SimpleNamespace(
            meme=RedditSub(reddit, "dankmemes"),
            aww=RedditSub(reddit, "aww"),
            ph=RedditSub(reddit, "ProgrammerHumor"),
            edgymemes=RedditSub(reddit, "EdgyMemes"),
            oneninetyfive=RedditSub(reddit, "195")
        )

    def getreddit(self, sub):
        reddit = sub.new(limit=100)
        sublist = []
        for child in reddit:
            if getattr(child, "post_hint", "not image") == "image":
                sublist.append(child.url)

        rndimage = random.choice(sublist)
        embed = discord.Embed()

        embed.set_image(url=rndimage)
        return embed

    @commands.command(brief="Makes you laugh (maybe)!")
    async def meme(self, ctx):
        """
            Get a random post from /r/meme

            Usage:
                {prefix}meme
        """
        await ctx.send(embed=self.sub_list.meme.get_rnd_item)

    @commands.command(brief="Awwwww!")
    async def aww(self, ctx):
        """
            Get a random post from /r/aww

            Usage:
                {prefix}aww
        """
        await ctx.send(embed=self.sub_list.aww.get_rnd_item)

    @commands.command(brief="Programmer Humor!")
    async def ph(self, ctx):
        """
            Get a random post from /r/ph

            Usage:
                {prefix}ph
        """
        await ctx.send(embed=self.sub_list.ph.get_rnd_item)

    @commands.command(brief="Edgy Memes!")
    async def edgy(self, ctx):
        """
            Get a random post from /r/ph

            Usage:
                {prefix}ph
        """
        await ctx.send(embed=self.sub_list.edgymemes.get_rnd_item)

    @commands.command(brief="Edgy Memes!")
    async def oneninefive(self, ctx):
        """
            Get a random post from /r/195

            Usage:
                {prefix}ph
        """
        await ctx.send(embed=self.sub_list.oneninetyfive.get_rnd_item)


def setup(bot, settings):
    bot.add_cog(Reddit(bot, settings))
