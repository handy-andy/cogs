import discord  # discord api
from discord.ext import commands  # commands extension
from discord.ext.commands.bot import _mentions_transforms, _mention_pattern

import config
import errors
import time

from lib.CogBase import CogBase
from lib.checks import is_owner
from settings import settings
from types import SimpleNamespace


class Core(CogBase):
    """
        This cog provides commands to help server managers control the bot
        They are provided as utility as to make manager's lives better
    """
    def __init__(self, bot):
        super().__init__(bot, 4, "Core")
        bot.remove_command("help")

        self.messages = [
            ":thumbsup: Success owo!",
            "That item either doesnt exist or is not disabled!",
            "That is an item that cannot be toggled!"
        ]

    async def __global_check_once(self, ctx):
        if ctx.command.cog_name == "Core" or \
           ctx.command.name == "help" or \
           ctx.command.cog_name == "Owner" and is_owner(ctx):
            return True

        if ctx.guild is None or ctx.channel.id in self.bot.servopts[ctx.guild.id].channels_restricted:
            raise errors.ChanRestricted()

        if ctx.command.cog_name.lower() in self.bot.servopts[ctx.guild.id].cogs_disabled or \
           ctx.command.name in self.bot.servopts[ctx.guild.id].cmds_disabled:
            raise errors.ModuleDisabled()

        return True

    async def __local_check(self, ctx):
        if ctx.guild is None:
            raise commands.NoPrivateMessage('This command cannot be used in private messages.')

        return ctx.channel.permissions_for(ctx.author).administrator or \
            ctx.channel.permissions_for(ctx.author).manage_guild or \
            all(p(ctx) for p in ctx.command.checks)

    async def create_db(self, server):
        # create database entry for new servers and servers already joined
        try:
            if not self.bot.db.exists("dino_servers", where={"server_id": server.id}):
                # get human member and bot count, for stats used later on
                members = filter(lambda m: not m.bot, server.members)
                bots = filter(lambda m: m.bot, server.members)

                # insert into database
                self.bot.db.insert("dino_servers", values={
                    "server_id": server.id,
                    "server_name": server.name,
                    "server_members": len(list(members)),
                    "server_bots": len(list(bots))
                })
                self.bot.servopts[server.id] = config.Config(self.bot.db, server.id)
        except Exception as e:
            await self.bot.send_error(f"- Database error on server: {server.id}\n", e)

    async def on_ready(self):
        guild_ids = []
        for guild in self.bot.guilds:
            guild_ids.append(str(guild.id))
            await self.create_db(guild)

        servers = self.bot.db.select('dino_servers', ['server_id'])
        for server in servers:
            if server['server_id'] not in guild_ids:
                await self.on_guild_remove(SimpleNamespace(id=server['server_id']))

    async def on_guild_join(self, guild):
        await self.create_db(guild)

    async def on_guild_remove(self, server):
        if self.bot.db.exists("dino_server_config", where={"server_id": server.id}):
            self.bot.db.delete("dino_server_config", where={"server_id": server.id})

        if self.bot.db.exists("dino_server_cogs_cmds_disabled", where={"server_id": server.id}):
            self.bot.db.delete("dino_server_cogs_cmds_disabled", where={"server_id": server.id})

        if self.bot.db.exists("dino_server_channels_restricted", where={"server_id": server.id}):
            self.bot.db.delete("dino_server_channels_restricted", where={"server_id": server.id})

        if self.bot.db.exists("dino_servers", where={"server_id": server.id}):
            self.bot.db.delete("dino_servers", where={"server_id": server.id})

    @commands.command(brief="Update your server prefix")
    @commands.check(lambda c: settings["core"].allow_custom_prefix)
    async def prefix(self, ctx, new_prefix):
        """
        This command is used to update the prefix for your guild.
        This command requires the setter to have at least Manage Guild permissions.

        Usage:
            {prefix}prefix <new_prefix:text>
        """
        self.bot.servopts[ctx.guild.id].prefix = new_prefix
        await ctx.send(":thumbsup: Success owo")

    @commands.command(brief="Restrict the bot out of the channel")
    async def rbot(self, ctx):
        """
            This command is used to toggle bot restriction on the current channel.
            This command requires the setter to have at least Manage Guild permissions.

            Usage:
                {prefix}rbot
        """
        # easy reference for below
        cperm = self.bot.servopts[ctx.guild.id].channels_restricted

        callback = cperm.remove if ctx.channel.id in cperm else cperm.append
        callback(ctx.channel.id)

        await ctx.send(":thumbsup: Success owo I am now %srestricted in this channel!" % (
            "un" if ctx.channel.id not in cperm else ""
        ))

    def cog_cmd_toggle(self, ctx, name, what, todo, action):
        name = name.lower()

        item = getattr(self.bot.servopts[ctx.guild.id], what + "s_disabled")

        list_enabled = []
        for c in self.bot.all_commands.values():
            iname = (c.cog_name if what == "cog" else c.name).lower()

            if c.cog_name in ['Owner', 'Core'] and iname == name:
                return 2

            if iname in item or iname in list_enabled:
                continue

            list_enabled.append(iname)

        if (not todo and name in list_enabled) or (todo and name in item):
            action(name, item)
            return 0

        return 1

    @commands.command(brief="Enable a cog on this server!")
    async def enable(self, ctx, item_type, item_name: str):
        """
        This command is used to enable a cog or command to be used on the server.
        This command requires the setter to have at least Manage Guild permissions.

        Usage:
            {prefix}enable cmd|cog <cmd_or_cog_name:text>
        """
        if item_type not in ["cog", "cmd"]:
            raise errors.EnablingInvalidModule(item_type)

        response = self.cog_cmd_toggle(ctx, item_name, item_type, True, lambda n, i: i.remove(n))
        await ctx.send(self.messages[response])

    @commands.command(brief="Disable a cog on this server!")
    async def enable(self, ctx, item_type, item_name: str):
        """
        This command is used to disable a cog or command from being usable on the server.
        This command requires the setter to have at least Manage Guild permissions.

        Usage:
            {prefix}disable cmd|cog <cmd_or_cog_name:text>
        """
        if item_type not in ["cog", "cmd"]:
            raise errors.EnablingInvalidModule(item_type)

        response = self.cog_cmd_toggle(ctx, item_name, item_type, True, lambda n, i: i.remove(n))
        await ctx.send(self.messages[response])

    @commands.command(brief="Display bot uptime")
    async def uptime(self, ctx):
        time_difference = int(time.time() - self.bot.uptime)
        time_string = time.strftime("%Hh %Mm %Ss", time.gmtime(time_difference))
        await ctx.send(f"Bot has been up for {time_string}")

    @commands.command(brief="Send a small message showing bot info")
    async def about(self, ctx):
        dev_info = self.bot.get_user(232191905394327562)
        await ctx.send(
            f"Hey! I'm {self.bot.user.name}, a bot made by {dev_info.name}!\n"
            f"I'm a little bot, spawned from a desire to help a friend.\n\n"
            f"I use many libraries and APIs to work, such as:\n"
            f"- Discord.PY for the bot base\n"
            f"- aiohttp for HTTP(S) work\n"
            f"- pillow for image manipulation\n"
            f"- pymysql for database communication\n"
            f"- <https://cleverbot.io/> for my `{ctx.prefix}ask` command\n"
            f"- <https://apilayer.net/> for my currency conversion and language detector\n"
            f"- <https://pixabay.com/> for the pictures used in my `{ctx.prefix}animal` command\n"
            f".. and many others!"
        )

    # this is ugly as fuck
    @commands.command(brief="Shows this message.")
    async def help(self, ctx, *commands: str):
        """
            This command is used to view documentation of cogs and commands

            Usage:
                {prefix}help [cog:text] [command:text]
        """
        bot = ctx.bot

        destination = ctx.message.author if bot.pm_help else ctx.message.channel

        async def send(content):
            if bot.formatter.cmdList == 1 and len(commands) == 0:
                # Dont show if no cogs
                return
            await destination.send(content)

        def repl(obj):
            return _mentions_transforms.get(obj.group(0), '')

        # help by itself just lists our own commands.
        if len(commands) == 0:
            pages = await bot.formatter.format_help_for(ctx, bot)
        elif len(commands) == 1:
            # try to see if it is a cog name
            name = _mention_pattern.sub(repl, commands[0])
            if name in bot.cogs:
                command = bot.cogs[name]
            else:
                command = bot.all_commands.get(name)
                if command is None:
                    await send(bot.command_not_found.format(name))
                    return

            pages = await bot.formatter.format_help_for(ctx, command)
        else:
            name = _mention_pattern.sub(repl, commands[0])
            command = bot.all_commands.get(name)
            if command is None:
                await send(bot.command_not_found.format(name))
                return

            for key in commands[1:]:
                try:
                    key = _mention_pattern.sub(repl, key)
                    command = command.all_commands.get(key)
                    if command is None:
                        await send(bot.command_not_found.format(key))
                        return
                except AttributeError:
                    await send(bot.command_has_no_subcommands.format(command, key))
                    return

            pages = await bot.formatter.format_help_for(ctx, command)

        if bot.pm_help is None:
            characters = sum(map(lambda l: len(l), pages))
            # modify destination based on length of pages.
            if characters > 1000:
                destination = ctx.message.author

        for page in pages:
            await send(page)

    async def _Core__error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("You need to present all arguments! Please use `{0.prefix}help {0.command}` to get helpful information.".format(ctx))
        elif isinstance(error, discord.Forbidden):
            await ctx.send(":thumbsdown: I do not have permissions to perform this action ;_;")
        elif isinstance(error, commands.MissingPermissions):
            await ctx.send(":thumbsdown: You don't have permissions to perform this action ;_;")
        elif isinstance(error, commands.BadArgument):
            await ctx.send(":thumbsdown: Please make sure all arguments are of the correct type! Use `{0.prefix}help {0.command}` to get helpful information".format(ctx))
        elif not isinstance(error, commands.CheckFailure):
            await ctx.send(":thumbsdown: An unknown error has occured! The admins have been notified of this")
            await self.bot.send_error("- Server: {0.guild.id}\n- Channel: {0.channel.id}".format(ctx), error)


def setup(bot):
    bot.add_cog(Core(bot))
