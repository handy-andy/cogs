import re
import discord
import aiohttp
import async_timeout
from types import SimpleNamespace
from discord.ext import commands
from lib.pcpp import Styles, get_build, format_build, _re, Builds
from lib.CogBase import CogBase
from errors import PCPPError


class PCPartPicker(CogBase):
    def __init__(self, bot):
        super().__init__(bot, 4, "PC Part Picker")

        self.saved_builds = []
        self.hw.invoke_without_command = True
        #self.builds = Builds(self.bot.connection)

    async def on_message(self, message):
        if not self.bot.ready or message.author.bot or not message.guild:
            return

        if message.channel.id in self.bot.servopts[message.guild.id].channels_restricted or\
           re.match(r".hw new ", message.content):
            return

        matches = []
        for (full, saved, pcpp_id) in _re.url.findall(message.content):
            build = await format_build(
                pcpp_id if saved == "list" else full,
                style="md_block",
                saved=saved != "list"
            )

            matches.append(build)

        if matches:
            for build in matches:
                for page in build:
                    await message.channel.send(page)

    @commands.group(brief="Setup and display hardware")
    async def hw(self, ctx, *, build_name_or_id: str = ""):
        """
        Display your main hardware build
        """
        build_name_or_id = build_name_or_id.split(" ")
        if build_name_or_id[0] in ["new", "get", "rename", "update", "delete", "main"]:
            return

        user = discord.utils.get(self.saved_builds, user_id=ctx.author.id)
        if not user or user and not user.builds:
            raise PCPPError("self_no_builds")

        try:
            build_id = int(build_name_or_id[0]) if build_name_or_id[0] != '' else user.main_build_id
        except ValueError:
            build = discord.utils.get(user.builds, name=" ".join(build_name_or_id))
        else:
            build = user.builds[build_id]

        for page in getattr(Styles, build.style, "md_block")(build.build):
            await ctx.send(page)

    @hw.command(brief="Set your main build")
    async def main(self, ctx, *, build_id: int):
        user = discord.utils.get(self.saved_builds, user_id=ctx.author.id)
        if not user or user and not user.builds:
            raise PCPPError("self_no_builds")

        if build_id < 1:
            raise PCPPError("id_error")

        try:
            build = user.builds[build_id - 1]
        except IndexError:
            raise PCPPError("build_noexists")
        else:
            index = build_id - 1
            user.main_build_id = index

            await ctx.send(f"Build successfully set to build {build.name}!")

    @hw.command(brief="Delete one of your builds from the database")
    async def delete(self, ctx, *, build_id: int):
        user = discord.utils.get(self.saved_builds, user_id=ctx.author.id)
        if not user or user and not user.builds:
            raise PCPPError("self_no_builds")

        if build_id < 1:
            raise PCPPError("id_error")

        index = build_id - 1
        try:
            build = user.builds[index]
        except IndexError:
            raise PCPPError("build_noexists")
        else:
            build_name = build.name
            if index == user.main_build_id:
                return await ctx.send("You cannot delete your main build!")

            del user.builds[index]
            await ctx.send(f"You have successfully deleted your build named `{build_name}`")

    @hw.command(brief="Update one of your builds")
    async def update(self, ctx, build_id: int, build_url):
        user = discord.utils.get(self.saved_builds, user_id=ctx.author.id)
        if not user:
            raise PCPPError("self_no_builds")

        pcpp_id = _re.url.match(build_url)
        if pcpp_id:
            raise PCPPError("pcpp_id_error")

        pcpp_id = pcpp_id.group(1)

        try:
            build = user.builds[build_id - 1]
        except IndexError:
            raise PCPPError("build_noexists")
        else:
            build.build = await get_build(pcpp_id)
            await ctx.send("Your build has been updated!")

    @hw.command(brief="Rename one of your builds")
    async def rename(self, ctx, build_id, *, new_name):
        user = discord.utils.get(self.saved_builds, user_id=ctx.author.id)
        if not user or user and not user.builds:
            raise PCPPError("self_no_builds")

        if build_id < 1:
            return await ctx.send("This is an invalid index!")

        try:
            index = build_id - 1
            build = user.builds[index]
        except IndexError:
            raise PCPPError("build_noexists")
        else:
            old_name = build.name
            build.name = new_name
            await ctx.send(f"You have successfully renamed your build from `{old_name}` to `{new_name}`")

    @hw.command(brief="Get a specific build from a member")
    async def get(self, ctx, member: discord.Member, *, id_or_name):
        user = discord.utils.get(self.saved_builds, user_id=member.id)
        if not user:
            raise PCPPError("no_builds")

        build = None
        try:
            build_id = int(id_or_name) - 1
        except ValueError:
            build = discord.utils.get(user.builds, name=id_or_name)
        else:
            build = user.builds[build_id]
        finally:
            if not build:
                raise PCPPError("build_noexists")

        for page in getattr(Styles, build.style, "md_block")(build.build):
            await ctx.send(page)

    @hw.command(brief="List your own or another user's builds")
    async def list(self, ctx, member: discord.Member = None):
        if not member:
            member = ctx.author

        user = discord.utils.get(self.saved_builds, user_id=member.id)
        if not user:
            raise PCPPError("no_builds")

        build_list = []
        for build_id, build in enumerate(user.builds):
            build_list.append(f"{build_id+1}: {build.name}")

        await ctx.send("Available builds:```\n"+("\n".join(build_list)) +
                       f"```Current main build ID: {user.main_build_id+1}")

    @hw.command(brief="Create a new hardware profile")
    async def new(self, ctx, build_url, *, build_name: str):
        """
        Store a PCPP profile to your Discord user

        Usage:
            {prefix}hw new <pcpp_url> <build_name>
        """
        user = discord.utils.get(self.saved_builds, user_id=ctx.author.id)
        if not user:
            self.saved_builds.append(SimpleNamespace(user_id=ctx.author.id, main_build_id=0, builds=[]))
            user = discord.utils.get(self.saved_builds, user_id=ctx.author.id)

        pcpp_id = _re.url.match(build_url)
        if not pcpp_id:
            raise PCPPError("pcpp_id_error")

        if build_name.isdigit():
            raise PCPPError("name_type_error")

        if discord.utils.get(user.builds, name=build_name):
            raise PCPPError("build_exists")

        build_id = pcpp_id[3]
        if pcpp_id[2] != "list":
            async with aiohttp.ClientSession() as session:
                async with async_timeout.timeout(15):
                    async with session.get(pcpp_id[1]) as response:
                        text = await response.text()

                        build_id = _re.saved.id.findall(text)
                        build_id = build_id[0]

        build = await get_build(build_id)
        user.builds.append(SimpleNamespace(name=build_name, build=build, style="md_block", link=build_url))

        await ctx.send("Your build has been stored!")

    async def _PCPartPicker__error(self, ctx, error):
        if isinstance(error, discord.ext.commands.errors.MissingRequiredArgument):
            await ctx.send({
                "build_id":     "You need to specify a saved build's ID",
                "build_url":    "You need to specify a PC Part Picker shared build URL",
                "build_name":   "You need to give your new build a name",
                "new_name":     "You need to give your build a name",
                "member":       "You need to mention a member whose build you wish to see",
                "id_or_name":   "You need to specify a build ID or a build name"
            }[error.param.name])
        elif isinstance(error, PCPPError):
            await ctx.send({
                "id_error":         "The provided ID is invalid!",
                "pcpp_id_error":    "You need to give a valid PC Part Picker share URL",
                "name_type_error":  "You may not name a build using only a number!",
                "build_exists":     "You already have a build saved under this name!",
                "build_noexists":   "This build doesn't exist!",
                "no_builds":        "This user currently has no builds on file",
                "self_no_builds":   "You don't have any builds saved!"
            }[error.code])
        else:
            await ctx.send(":thumbsdown: An unknown error has occured! The admins have been notified of this")
            await self.bot.send_error("- Server: {0.guild.id}\n- Channel: {0.channel.id}".format(ctx), error)


def setup(bot):
    bot.add_cog(PCPartPicker(bot))
